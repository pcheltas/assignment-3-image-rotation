#include "../include/rotations.h"


static struct pair one_pixel_rotate(int32_t i, int32_t j, int16_t angle, struct image new_image) {
    uint32_t new_j = 0;
    uint32_t new_i = 0;

    if (angle == 90) {
        new_j = j;
        new_i = new_image.height - i - 1;
    }
    if (angle == 180) {
        new_j = new_image.width - i - 1;
        new_i = new_image.height - j - 1;
    }
    if (angle == 270) {
        new_j = new_image.width - j - 1;
        new_i = i;
    }

    return (struct pair) {.i = new_i, .j=new_j};
}

struct rotate_image unknown_angle(struct image *image){
    (void) image;
    return (struct rotate_image) {.status=ROTATION_ERROR};
}

        struct rotate_image rotate_0(struct image *image) {
    struct image new_image;

    new_image.width = image->width;
    new_image.height = image->height;

    new_image.data = (struct pixel *) malloc(new_image.width * new_image.height * sizeof(struct pixel));


    for (int32_t i = 0; i < image->width; i++) {
        for (int32_t j = 0; j < image->height; j++) {
            new_image.data[j * image->width + i] = image->data[j * image->width + i];
        }

    }

    return (struct rotate_image) {.image_to_rotate=new_image, .status=ROTATION_OK};
}

struct rotate_image rotate_90(struct image *image) {
    struct image new_image;

    new_image.width = image->height;
    new_image.height = image->width;

    new_image.data = (struct pixel *) malloc(new_image.width * new_image.height * sizeof(struct pixel));


    for (int32_t i = 0; i < image->width; i++) {
        for (int32_t j = 0; j < image->height; j++) {
            struct pair pair = one_pixel_rotate(i, j, 90, new_image);
            struct pixel current_pixel = image->data[j * image->width + i];

            uint32_t new_j = pair.j;
            uint32_t new_i = pair.i;

            new_image.data[new_i * new_image.width + new_j] = current_pixel;

        }

    }

    return (struct rotate_image) {.image_to_rotate=new_image, .status=ROTATION_OK};
}


struct rotate_image rotate_180(struct image *image) {
    struct image new_image;

    new_image.width = image->width;
    new_image.height = image->height;

    new_image.data = (struct pixel *) malloc(new_image.width * new_image.height * sizeof(struct pixel));


    for (int32_t i = 0; i < image->width; i++) {
        for (int32_t j = 0; j < image->height; j++) {
            struct pair pair = one_pixel_rotate(i, j, 180, new_image);
            struct pixel current_pixel = image->data[j * image->width + i];

            uint32_t new_j = pair.j;
            uint32_t new_i = pair.i;

            new_image.data[new_i * new_image.width + new_j] = current_pixel;

        }

    }

    return (struct rotate_image) {.image_to_rotate=new_image, .status=ROTATION_OK};
}

struct rotate_image rotate_270(struct image *image) {
    struct image new_image;

    new_image.width = image->height;
    new_image.height = image->width;

    new_image.data = (struct pixel *) malloc(new_image.width * new_image.height * sizeof(struct pixel));


    for (int32_t i = 0; i < image->width; i++) {
        for (int32_t j = 0; j < image->height; j++) {
            struct pair pair = one_pixel_rotate(i, j, 270, new_image);
            struct pixel current_pixel = image->data[j * image->width + i];

            uint32_t new_j = pair.j;
            uint32_t new_i = pair.i;

            new_image.data[new_i * new_image.width + new_j] = current_pixel;

        }

    }

    return (struct rotate_image) {.image_to_rotate=new_image, .status=ROTATION_OK};
}

static struct rotate_image rotate_angle(struct rotate_image (f)(struct image *), struct rotate_image img) {
    return f(&img.image_to_rotate);
}

angle_function get_function_by_angle(int16_t angle){
    switch (angle) {
        case 0: {
            return &rotate_0;
        }
        case 90:
        case -270: {
            return &rotate_90;
        }

        case 180:
        case -180: {
            return &rotate_180;
        }

        case 270 :
        case -90: {
            return &rotate_270;
        }

        default: {
            return &unknown_angle;
        }
    }
}

struct rotate_image rotate(struct rotate_image image) {
    int16_t angle = image.angle;

    struct rotate_image (*f_pointer)(struct image *) = get_function_by_angle(angle);

    struct rotate_image return_image = rotate_angle(f_pointer, image);

    return return_image;
}

