#include "../include/util.h"


uint32_t calc_padding(uint64_t width) {
    return (4 - width * sizeof(struct pixel) % 4) % 4;
}

struct correct_angle parse_angle(char *string) {
    char *end = "";

    int16_t angle = (int16_t) strtoll(string, &end, 10);

    if (*end != '\0') {
        return (struct correct_angle) {.flag = 1};
    }

    return (struct correct_angle) {.angle = angle, .flag=0};

}

void print_error(char *message) {
    fprintf(stderr, "%s", message);
}

void check_error(uint8_t status, uint8_t correct_status, char* message){
    if (status != correct_status){
        print_error(message);
        exit(1);
    }
}
