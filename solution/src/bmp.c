#include "../include/bmp.h"

#define BMP_TYPE 0x4D42
#define BMP_BISIZE 40
#define BMP_BIBIT_CNT 24
#define BMP_BIPLANES 1

static enum read_status read_bmp(struct image* img, FILE* file){
    uint64_t padding = calc_padding(img->width);
    size_t pixel_size = sizeof(struct pixel);
    for (uint64_t i = 0; i < img->height; ++i) {
        for (uint64_t j = 0; j < img->width; ++j) {
            if (fread(&img->data[i * img->width + j], 1, pixel_size, file) != pixel_size) {
                free(img->data);
                return READ_INVALID_BITS;
            }

        }
        fseek(file, (long) padding, SEEK_CUR);
    }

    return READ_OK;
}


enum read_status from_bmp( struct bmp_image bmp_image){
    FILE* in = bmp_image.bmp_file;
    struct image *img = bmp_image.bmp_image;


    struct bmp_header header = {0};
    size_t read_bytes_num = fread(&header, sizeof(struct bmp_header), 1, in);
    if (!read_bytes_num){
        return READ_INVALID_HEADER;
    }

    if (header.bfType != BMP_TYPE) {
        return READ_INVALID_SIGNATURE;
    }

    img->width = (uint64_t)header.biWidth;
    img->height = (uint64_t)header.biHeight;
    img->data = malloc(sizeof(struct pixel) * img->width * img->height);
    if (img->data == NULL) {
        return READ_INVALID_BITS;
    }

    fseek(in, header.bOffBits, SEEK_SET);

    if (read_bmp(img, in) != READ_OK){
        return READ_INVALID_BITS;
    }

    return READ_OK;
}

static enum write_status write_header(struct bmp_header header, FILE* out){
    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)) {
        return WRITE_ERROR;
    }
    return WRITE_OK;
}

static enum write_status write_bmp(struct image* image, FILE* file){
    for (uint32_t y = 0; y < image -> height; y++) {
        if (fwrite(&image -> data[image -> width * y], sizeof(struct pixel), image -> width, file) != image -> width) {
            return WRITE_ERROR;
        }
        fseek(file, calc_padding(image->width), SEEK_CUR);
    }

    return WRITE_OK;
}

enum write_status to_bmp( struct bmp_image bmp_image){
    struct image* image = bmp_image.bmp_image;
    FILE* file = bmp_image.bmp_file;

    uint32_t padding = calc_padding(image->width);

    struct bmp_header header = (struct bmp_header) {
            .bfType = BMP_TYPE,
            .bfileSize = sizeof(struct bmp_header) + image->width * image->height * sizeof(struct pixel)
                                                       + padding * image->height,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BMP_BISIZE,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = BMP_BIPLANES,
            .biBitCount = BMP_BIBIT_CNT,
            .biCompression = 0,
            .biSizeImage = image->width * image->height * sizeof(struct pixel) + padding * image->height,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };

    if (write_header(header, file) != WRITE_OK){
        return WRITE_ERROR;
    }

    if (write_bmp(image, file) != WRITE_OK){
        return WRITE_ERROR;
    }

    return WRITE_OK;
}

