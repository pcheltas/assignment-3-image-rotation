#include "../include/bmp.h"
#include "../include/file_io.h"
#include "../include/rotations.h"
#include <stdio.h>

int main(int argc, char **argv) {
    if (argc != 4) {
        print_error("Wrong numbers of args");
        return 1;
    }

    struct correct_angle correct_angle = parse_angle(argv[3]);

    check_error(correct_angle.flag, 0, "Invalid angle");

    int16_t angle = correct_angle.angle;

    FILE *input_file = open_file_read(argv[1]);
    struct image src_image = {0};

    struct bmp_image bmp_image = {.bmp_file = input_file, .bmp_image = &src_image};

    uint8_t status = from_bmp(bmp_image);

    check_error(status, READ_OK, "Could not read file");

    FILE *output_file = file_write(argv[2]);

    bmp_image.bmp_file = output_file;

    struct rotate_image output_image = {.image_to_rotate = *bmp_image.bmp_image, .angle=angle};

    struct rotate_image rotate_image = rotate(output_image);


    check_error(rotate_image.status, ROTATION_OK, "Rotation error");


    free(bmp_image.bmp_image->data);

    bmp_image.bmp_image = &rotate_image.image_to_rotate;
    status = to_bmp(bmp_image);

    check_error(status, WRITE_OK, "Could not write file");


    status = close_file(input_file);

    check_error(status, INTERACTION_OK, "Could not close file");


    status = close_file(output_file);

    check_error(status, INTERACTION_OK, "Could not close file");


    free(bmp_image.bmp_image->data);


    puts("DONE");

}
