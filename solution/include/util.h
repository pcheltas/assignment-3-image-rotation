#ifndef IMAGE_TRANSFORMER_UTIL_H
#define IMAGE_TRANSFORMER_UTIL_H

#include "../include/image.h"
#include <stdio.h>
#include <stdlib.h>


struct correct_angle{
    int16_t angle;
    size_t flag;
};

uint32_t calc_padding(uint64_t width);

void print_error(char *message);

void check_error(uint8_t status, uint8_t correct_status, char* message);

struct correct_angle parse_angle(char *string);

#endif //IMAGE_TRANSFORMER_UTIL_H
