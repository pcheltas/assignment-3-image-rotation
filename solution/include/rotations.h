#ifndef TEST_ROTATIONS_H
#define TEST_ROTATIONS_H

#include "../include/bmp.h"

enum rotation_status {
    ROTATION_OK = 0,
    ROTATION_ERROR,
};


struct rotate_image {
    struct image image_to_rotate;
    int16_t angle;
    enum rotation_status status;
};

struct pair {
    uint32_t i;
    uint32_t j;
};
typedef struct rotate_image (*angle_function)(struct image *);
struct rotate_image rotate(struct rotate_image image); // тут вызывается rotate_angle

#endif
