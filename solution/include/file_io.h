#ifndef IMAGE_TRANSFORMER_FILE_IO_H
#define IMAGE_TRANSFORMER_FILE_IO_H
//enum read_status  {
//    READ_OK = 0,
//    READ_INVALID_SIGNATURE,
//    READ_INVALID_BITS,
//    READ_INVALID_HEADER
//    /* коды других ошибок  */
//};


enum file_interaction {
    INTERACTION_OK = 0,
    INTERACTION_ERROR,
};

//
//#include <stdbool.h>
#include <stdio.h>
FILE*  open_file_read(const char* name);
FILE*  file_write(const char* name);
enum file_interaction close_file(FILE* file);
#endif //IMAGE_TRANSFORMER_FILE_IO_H
